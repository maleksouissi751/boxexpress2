import 'package:ex123/pages/log_in.dart';
import 'package:ex123/pages/splashscreen.dart';
import 'package:ex123/sidebar/sidebar_layout.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white, primaryColor: Colors.white),
      home: MySplashScreen(),
      routes: <String, WidgetBuilder>{
        '/Login': (BuildContext context) => new LogIn(),
        '/HomePage': (BuildContext context) => new SideBarLayout(),
      },
    );
  }
}
