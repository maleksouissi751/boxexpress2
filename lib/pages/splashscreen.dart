import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

class MySplashScreen extends StatefulWidget {
  @override
  _MySplashScreenState createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen> {
  bool _isLoggedIn = false;

  @override
  void _checkIfLoggedIn() async {
    // check if token is t here
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    print(token);
    if (token != null) {
      setState(() {
        _isLoggedIn = true;
      });
      Navigator.of(context).pushReplacementNamed("/HomePage");
    }
    print(_isLoggedIn);
  }

  void initState() {
    //_checkIfLoggedIn();
    //print(_isLoggedIn);
//    _mockCheckForSession().then((status) {
//      if (status) {
//        navigateUser();
//      }
//    });

    startTimer();
    super.initState();
  }

  void startTimer() {
    Timer(Duration(milliseconds: 1500), () {
      navigateUser(); //It will redirect  after 3 seconds
    });
  }

  void navigateUser() {
    _checkIfLoggedIn();

    if (_isLoggedIn == true) {
      print('user already logged in ');
      Navigator.of(context).pushReplacementNamed("/HomePage");
    } else {
      print('user didnt loggedIn yet');
      Navigator.of(context).pushReplacementNamed("/Login");
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color(0xFF3B988E), Color(0xFFFCCA73)],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: size.width * 0.8,
                  child: Image(
                    image: AssetImage('assets/logo2.png'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
