import 'package:ex123/bloc/navigation_bloc/navigation_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import '../bloc.navigation_bloc/navigation_bloc.dart';
import 'package:ex123/widget/my-input-field.dart';
import 'package:ex123/widget/login-button.dart';

class DetailsPage extends StatefulWidget with NavigationStates {
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  //FocusNode myFocusNode = new FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F9F9),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical:150, horizontal: 50),
                child: Form(
                  child: Column(
                    children: [
                      MyInputField(
                        text: "Code",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyInputField(
                        text: "Nom du client",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyInputField(
                        text: "Numéro du client",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyInputField(
                        text: "Adress",
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      LogInButton(
                        press: () {},
                        text: "Edit Profile",
                        height: 0.08,
                        width: 0.6,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
