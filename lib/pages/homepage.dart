import 'package:ex123/bloc/navigation_bloc/navigation_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ex123/widget/rounded_button.dart';
import 'detailspage.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'dart:io' show Platform;
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'detailspage.dart';

class HomePage extends StatefulWidget with NavigationStates {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScanResult scanResult;
  ScaffoldState scaffoldState;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    ));
  }

  final _flashOnController = TextEditingController(text: "Flash on");
  final _flashOffController = TextEditingController(text: "Flash off");
  final _cancelController = TextEditingController(text: "Cancel");

  var _aspectTolerance = 0.00;
  var _numberOfCameras = 0;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = [..._possibleFormats];

  @override
  // ignore: type_annotate_public_apis
  initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      _numberOfCameras = await BarcodeScanner.numberOfCameras;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Color(0xFFF6F9F9),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Column(
              children: [
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: 25, left: 40),
                    width: size.width * 0.7,
                    child: Image(image: AssetImage('assets/logo1.png')),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/pallet.png',
                  text: "Au Depot",
                  press: scan_instock,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/time.png',
                  text: "En Cours",
                  press: scan_waiting,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/cancel.png',
                  text: "Livraison \n Echouée",
                  press: scan_canceldelivery,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/delivered.png',
                  text: "Livré",
                  press: scan_delivered,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/returned.png',
                  text: "Retour Au\n Depot",
                  press: scan_stockreturn,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/returned.png',
                  text: "Retour Définitif",
                  press: scan_definitifreturn,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/returned.png',
                  text: "Retour Au \nFournisseur",
                  press: scan_fournisseurreturn,
                ),
                RoundedButton(
                  height: 0.16,
                  image: 'assets/info.png',
                  text: "Infos",
                  press: scan_infos,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future scan_delivered() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '4'},
      );
      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("colis livré avec succès.");
  }

  Future scan_instock() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '1'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("colis en dépot.");
  }

  Future scan_definitifreturn() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '6'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("colis en retour .");
  }

  Future scan_waiting() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '2'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("colis en attente .");
  }

  Future scan_canceldelivery() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '3'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("livraison echouée .");
  }

  Future scan_stockreturn() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '5'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("cretour en stock .");
  }

  Future scan_fournisseurreturn() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.put(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
        body: {'state': '7'},
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    _showScaffold("retour au fournisseur .");
  }

  Future scan_infos() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": "annuler",
          "flash_on": "flash",
          "flash_off": "flash off",
        },
        useCamera: -1,
        autoEnableFlash: false,
        android: AndroidOptions(
          aspectTolerance: 0.00,
          useAutoFocus: true,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);

      setState(() => scanResult = result);
      final http.Response response = await http.get(
        'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
      );

      final responseJson = json.decode(response.body);
      print('Response: ${responseJson}');
      print(result.rawContent);
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
    navigate_to_info(scanResult);
  }

  void navigate_to_info(scanResult) {
    show_popup(scanResult);
  }

  void show_popup(ScanResult result) async {
    final http.Response response = await http.get(
      'https://aftercode-tms.herokuapp.com/api/orders/${result.rawContent}',
    );

    final responseJson = json.decode(response.body);

    final name = responseJson['name'];
    final adress = responseJson['address'];
    final phone1 = responseJson['phone1'];
    final phone2 = responseJson['phone2'];
    final price = responseJson['amount_including_tax'];
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Stack(overflow: Overflow.visible, children: <Widget>[
              Positioned(
                right: -40.0,
                top: -40.0,
                child: InkResponse(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: CircleAvatar(
                    child: Icon(Icons.close),
                    backgroundColor: Color(0XFF3B988E),
                  ),
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Name: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          name,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Adress: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          adress,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Phone1: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          phone1,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Phone2: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          phone2,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          'Prix: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          price,
                          style: TextStyle(
                            fontSize: 15,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ]),
          );
        });
  }
}
