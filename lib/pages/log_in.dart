import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ex123/sidebar/sidebar_layout.dart';
import 'package:ex123/widget/inputwithicon.dart';
import 'package:ex123/widget/login-button.dart';
import 'package:ex123/api/api.dart';
import 'dart:convert';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  bool _isLoading = false;

  TextEditingController EmailController = TextEditingController();

  TextEditingController PasswordController = TextEditingController();
  ScaffoldState scaffoldState;
  _showMsg(msg) {
    //
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F9F9),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin:
                  EdgeInsets.only(top: 100, left: 30, right: 30, bottom: 70),
              child: Image(
                image: AssetImage('assets/delivery.png'),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                children: <Widget>[
                  InputWithIcon(
                    controller: EmailController,
                    icon: Icons.email,
                    hint: "Enter Email...",
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  InputWithIcon(
                    controller: PasswordController,
                    icon: Icons.vpn_key,
                    hint: "Enter Password...",
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  LogInButton(
                    height: 0.07,
                    width: 0.8,
                    text: _isLoading ? 'Loging...' : 'Login',
                    press: _isLoading ? null : _login,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'email': EmailController.text,
      'password': PasswordController.text
    };

    var res = await CallApi().postData(data, 'login');
    var body = json.decode(res.body);
    if (body['status_code'] == 200) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.setString('token', body['access_token']);
      localStorage.setString('user', json.encode(body['user']));
      print(body);
      Navigator.push(context,
          new MaterialPageRoute(builder: (context) => SideBarLayout()));
    } else {
      _showMsg(body['message']);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
