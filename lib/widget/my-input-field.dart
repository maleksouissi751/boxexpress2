import 'package:flutter/material.dart';
import 'package:ex123/api/api.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MyInputField extends StatefulWidget {
  final String text,hint;
  const MyInputField({Key key, this.text, this.hint}) : super(key: key);
  //MyInputField(this.text);
//  MyInputField({Key key, this.text}) : super(key: key);
  @override
  _MyInputFieldState createState() => _MyInputFieldState();
}

class _MyInputFieldState extends State<MyInputField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: widget.text,
        labelStyle: TextStyle(
          color: Color(0xFF797979),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF797979)),
        ),
        hintText: widget.hint,
      ),
    );
  }
}
