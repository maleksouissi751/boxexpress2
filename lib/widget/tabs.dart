import 'package:flutter/material.dart';
import 'package:ex123/widget/listOrders.dart';

class NestedTabBar extends StatefulWidget {
  @override
  _NestedTabBarState createState() => _NestedTabBarState();
}

class _NestedTabBarState extends State<NestedTabBar>
    with TickerProviderStateMixin {
  TabController _nestedTabController;

  @override
  void initState() {
    super.initState();

    _nestedTabController = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _nestedTabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(30.0),
            topRight: const Radius.circular(30.0),
          )),
      height: MediaQuery.of(context).size.height * 0.85,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          TabBar(
            controller: _nestedTabController,
            indicator: BoxDecoration(
                borderRadius: BorderRadius.circular(50.0),
                color: Color(0XFFF6F9F9)),
            labelColor: Color(0XFF2D4D58),
            labelStyle: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),
            unselectedLabelColor: Color(0XFF2D4D58),
            isScrollable: true,
            tabs: <Widget>[
              Tab(
                text: "Delivered",
              ),
              Tab(
                text: "Returned",
              ),
              Tab(
                text: "waiting",
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0),
                )),
            height: screenHeight * 0.75,
            // margin: EdgeInsets.only(left: 16.0, right: 16.0),
            child: TabBarView(
              controller: _nestedTabController,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    //borderRadius: BorderRadius.circular(8.0),
                    color: Color(0XFFF6F9F9),
                  ),
                  child: Listorders(),
                ),
                Container(
                  decoration: BoxDecoration(
                    //borderRadius: BorderRadius.circular(8.0),
                    color: Color(0XFFF6F9F9),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    //borderRadius: BorderRadius.circular(8.0),
                    color: Color(0XFFF6F9F9),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
